<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->boolean('visited');
            $table->timestamps();
        });

        DB::table('places')->insert([
            ['name' => 'Tokyo', 'visited' => FALSE],
            ['name' => 'Budapest', 'visited' => TRUE],
            ['name' => 'Nairobi', 'visited' => FALSE],
            ['name' => 'Berlin', 'visited' => TRUE],
            ['name' => 'Lisbon', 'visited' => TRUE],
            ['name' => 'Denver', 'visited' => FALSE],
            ['name' => 'Moscow', 'visited' => FALSE],
            ['name' => 'Helsinki', 'visited' => TRUE],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
